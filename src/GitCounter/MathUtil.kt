package GitCounter

import com.google.gson.Gson

class MathUtil {
    fun multiply(vararg numbers: Int): Int {
        var result = 1;
        for (i in numbers)
            result *= i
        return result
    }

    fun divide(vararg numbers: Int): Int {
        var result = numbers[0];
        for (i in numbers)
            result /= i
        return result
    }

    fun plus(vararg numbers: Int): Int {
        var result = 0;
        for (i in numbers)
            result += i
        return result
    }

    fun minus(vararg numbers: Int): Int {
        var result = numbers[0];
        for (i in numbers)
            result -= i
        return result
    }
}

