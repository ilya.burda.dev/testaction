package com.company

class ExampleTest {
    fun sort(strings: Array<String>, condition: (String, String) -> Int) {
        var counter = 1
        while (counter > 0) {
            counter = 0
            for (i in 0..strings.size - 2) {
                if (condition(strings[i], strings[i + 1]) > 0) {
                    var temp = strings[i]
                    strings[i] = strings[i + 1]
                    strings[i + 1] = temp
                    counter++
                }
            }
        }
        println(strings.contentToString())
    }

    fun getMax(numbers: Array<Int>, condition: (Int, Int) -> Int): Int {
        var indexMax = 0
        for (i in numbers.indices) {
            if (condition(numbers[i], numbers[indexMax]) > 0) {
                indexMax = i
            }
        }
        println(numbers[indexMax])
        return numbers[indexMax]
    }
}

fun main() {
    var exampleTest = ExampleTest()
    var listString = arrayListOf<String>("telefonation", "celebration", "generation")
    println(findSameSubstring(listString))
//    var array = arrayOf("khjkjjeyaa", "list", "to", "carbon")
//    var ints = arrayOf(1, 25, 2, 15, 132, 12, 42, 1)
//    println(calcLetter("aarwwwaa", "r"))
//    exampleTest.sort(array) { w: String, y: String ->
//        calcLetter(w, "a") - calcLetter(y, "a")
//    }
//    exampleTest.sort(array) { w: String, y: String -> y.length - w.length }
//    exampleTest.sort(array) { w: String, y: String ->
//        var constants = "qwrtpsdfghjklzxcvbnm"
//        var result1 = 0
//        var result2 = 0
//        for (i in constants.toCharArray()) {
//            result1 += calcLetter(w, i.toString())
//            result2 += calcLetter(y, i.toString())
//        }
//        result1 - result2
//    }
//    exampleTest.getMax(ints) { w: Int, y: Int ->
//        w - y
//    }
//    exampleTest.getMax(ints) { w: Int, y: Int ->
//        var result = 0
//        if (w % 5 == 0 && y % 5 == 0) {
//            result = w - y
//        } else if (w % 5 == 0 && y % 5 != 0) {
//            result = w
//        } else {
//            result = y
//        }
//        result
//    }
//    exampleTest.getMax(ints) { w: Int, y: Int ->
//        calcSum(w) - calcSum(y)
//    }
}

fun calcSum(y: Int): Int {
    var ints = y.toString()
    var result = 0
    for (i in ints) {
        result += Char.toString().toInt()
        println()
    }
    return result
}

fun calcLetter(word: String, letter: String): Int {
    return word.count { it == letter[0] }
}

fun findSameSubstring(list: List<String>): String {
    var result = ""
    var tempListSubstr: List<String> = generateSubstr(list[0])
    for (i in tempListSubstr) {
        if (list.count { it.indexOf(i) != -1 } == list.size) {
            if (i.length > result.length)
                result = i
        }
    }
    return result
}

fun generateSubstr(word: String): List<String> {
    var result = mutableListOf<String>()
    for (i in word.indices) {
        result.addAll(getSubList(i, word))
    }
    return result
}

fun getSubList(i: Int, word: String): Collection<String> {
    var result = mutableListOf<String>()
    for (j in i .. word.length) {
        var k = 1
        while (k < word.length - j ) {
            result.add(word.substring(j, k))
            k++
        }
    }
    return result
}
